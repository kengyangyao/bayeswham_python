#!/bin/bash

wham_dir=$1
periodicity=[$2,$2] 
periodicity_UMB=[$3,$3]
whether_reweight=$4
target_folder=${wham_dir}
dim=2
T=300
harmonicBiasesFile="${wham_dir}/bias/harmonic_biases.txt"
histBinEdgesFile="${wham_dir}/hist/hist_binEdges.txt"
histDir="${wham_dir}/hist"
tol_WHAM=1E-15 
maxIter_WHAM=1E6 
steps_MH=1E4 
saveMod_MH=1E3 
printMod_MH=1E3 
maxDpStep_MH=5E-4 
seed_MH=200184 
prior="none" 
alpha=1.0 

python BayesWHAM.py $dim $periodicity $T $harmonicBiasesFile $histBinEdgesFile $histDir $tol_WHAM $maxIter_WHAM $steps_MH $saveMod_MH $printMod_MH $maxDpStep_MH $seed_MH $prior $alpha

if [[ ${whether_reweight} == "1" ]]; then
    harmonicBiasesFile_UMB=${wham_dir}/bias/harmonic_biases.txt
    histBinEdgesFile_UMB="${wham_dir}/hist/hist_binEdges.txt"
    histDir_UMB="${wham_dir}/hist"
    fMAPFile_UMB="f_MAP.txt"
    fMHFile_UMB="f_MH.txt"
    trajDir_PROJ="${wham_dir}/traj_proj"
    trajDir_UMB="${wham_dir}/traj"
    histBinEdgesFile_PROJ="${wham_dir}/hist/hist_binEdges_proj.txt"
    python BayesReweight.py $T $dim $periodicity_UMB $harmonicBiasesFile $trajDir_UMB $histBinEdgesFile_UMB $histDir_UMB $fMAPFile_UMB $fMHFile_UMB $trajDir_PROJ $histBinEdgesFile_PROJ
fi

if [[ ! -d ${target_folder} ]]; then 
    mkdir -p ${target_folder}
fi
mv *.txt ${target_folder}
