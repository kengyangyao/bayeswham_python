import matplotlib
matplotlib.use("agg")
import os, re, sys, time
import random, math
import numpy as np
import numpy.matlib
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt

target_folder = sys.argv[1]

cmap_setting = 'jet'
temp_font_size = 25
colorbar_list = [[None,None] for item in range(4)]

f__hist_binCenters = target_folder + '/hist_binCenters.txt'
f__hist_binWidths = target_folder + '/hist_binWidths.txt'
f__pdf_MAP = target_folder + '/pdf_MAP.txt'
f__betaF_MAP = target_folder + '/betaF_MAP.txt'
f__f_MAP = target_folder + '/f_MAP.txt'
f__pdf_MH = target_folder + '/pdf_MH.txt'
f__betaF_MH = target_folder + '/betaF_MH.txt'
f__f_MH = target_folder + '/f_MH.txt'
f__logL_MH = target_folder + '/logL_MH.txt'
f__step_MH = target_folder + '/step_MH.txt'

binC = []
with open(f__hist_binCenters,'r') as fin:
    for line in fin:
        binC.append(line.strip().split())
binC = [[float(y) for y in x] for x in binC]
binC = np.array(binC)

binW = []
with open(f__hist_binWidths,'r') as fin:
    for line in fin:
        binW.append(line.strip().split())
binW = [[float(y) for y in x] for x in binW]
binW = np.array(binW)

fin = open(f__pdf_MAP,'r')
line = fin.readline()
pdf_MAP = line.strip().split()
pdf_MAP = [float(x) for x in pdf_MAP]
pdf_MAP = np.array(pdf_MAP)
fin.close()

fin = open(f__betaF_MAP,'r')
line = fin.readline()
betaF_MAP = line.strip().split()
betaF_MAP = [float(x) for x in betaF_MAP]
betaF_MAP = np.array(betaF_MAP)
fin.close()

fin = open(f__f_MAP,'r')
line = fin.readline()
f_MAP = line.strip().split()
f_MAP = [float(x) for x in f_MAP]
f_MAP = np.array(f_MAP)
fin.close()

pdf_MH = []
with open(f__pdf_MH,'r') as fin:
    for line in fin:
        pdf_MH.append(line.strip().split())
pdf_MH = [[float(y) for y in x] for x in pdf_MH]
pdf_MH = np.array(pdf_MH)

betaF_MH = []
with open(f__betaF_MH,'r') as fin:
    for line in fin:
        betaF_MH.append(line.strip().split())
betaF_MH = [[float(y) for y in x] for x in betaF_MH]
betaF_MH = np.array(betaF_MH)

f_MH = []
with open(f__f_MH,'r') as fin:
    for line in fin:
        f_MH.append(line.strip().split())
f_MH = [[float(y) for y in x] for x in f_MH]
f_MH = np.array(f_MH)

logL_MH = []
with open(f__logL_MH,'r') as fin:
    for line in fin:
        logL_MH.append(line.strip())
logL_MH = [float(x) for x in logL_MH]
logL_MH = np.array(logL_MH)

step_MH = []
with open(f__step_MH,'r') as fin:
    for line in fin:
        step_MH.append(line.strip())
step_MH = [float(x) for x in step_MH]
step_MH = np.array(step_MH)

fig_formal, axes_formal = plt.subplots(4, 2)
X,Y = np.meshgrid(binC[0],binC[1])
Z = np.reshape(betaF_MAP,(len(binC[0]),len(binC[1])))
Zmin = np.amin(Z[~np.isnan(Z)])
Zmax = np.amax(Z[~np.isnan(Z)])
Z[np.isnan(Z)] = 0
ax = axes_formal[0][0]
im = ax.contour(X, Y, Z.T, 40, cmap=cmap_setting, levels=np.arange(-15, 2).tolist())
colorbar_list[0][0] = fig_formal.colorbar(im, ax=ax)

betaF_MH_std = np.std(betaF_MH, axis=0)

X,Y = np.meshgrid(binC[0],binC[1])
Z = np.reshape(betaF_MH_std,(len(binC[0]),len(binC[1])))
Zmin = np.amin(Z[~np.isnan(Z)])
Zmax = np.amax(Z[~np.isnan(Z)])
Z[np.isnan(Z)] = 0
ax = axes_formal[0][1]
im = ax.contour(X, Y, Z.T, 40, cmap=cmap_setting)
colorbar_list[0][1] = fig_formal.colorbar(im, ax=ax)

#########################################

f__hist_binCenters = target_folder + '/hist_binCenters_PROJ.txt'
f__hist_binWidths = target_folder + '/hist_binWidths_PROJ.txt'
f__pdf_MAP = target_folder + '/pdf_PROJ_MAP.txt'
f__betaF_MAP = target_folder + '/betaF_PROJ_MAP.txt'
f__pdf_MH = target_folder + '/pdf_PROJ_MH.txt'
f__betaF_MH = target_folder + '/betaF_PROJ_MH.txt'

binC = []
with open(f__hist_binCenters,'r') as fin:
    for line in fin:
        binC.append(line.strip().split())
binC = [[float(y) for y in x] for x in binC]
binC = np.array(binC)

binW = []
with open(f__hist_binWidths,'r') as fin:
    for line in fin:
        binW.append(line.strip().split())
binW = [[float(y) for y in x] for x in binW]
binW = np.array(binW)

fin = open(f__pdf_MAP,'r')
line = fin.readline()
pdf_MAP = line.strip().split()
pdf_MAP = [float(x) for x in pdf_MAP]
pdf_MAP = np.array(pdf_MAP)
fin.close()

fin = open(f__betaF_MAP,'r')
line = fin.readline()
betaF_MAP = line.strip().split()
betaF_MAP = [float(x) for x in betaF_MAP]
betaF_MAP = np.array(betaF_MAP)
fin.close()

pdf_MH = []
with open(f__pdf_MH,'r') as fin:
    for line in fin:
        pdf_MH.append(line.strip().split())
pdf_MH = [[float(y) for y in x] for x in pdf_MH]
pdf_MH = np.array(pdf_MH)

betaF_MH = []
with open(f__betaF_MH,'r') as fin:
    for line in fin:
        betaF_MH.append(line.strip().split())
betaF_MH = [[float(y) for y in x] for x in betaF_MH]
betaF_MH = np.array(betaF_MH)

ground_truth_betaF_MAP = np.loadtxt('output/ground_truth/betaF_MAP.txt')
ground_truth_betaF_MH = np.loadtxt('output/ground_truth/betaF_MH.txt')
ground_truth_betaF_MAP = np.reshape(ground_truth_betaF_MAP,(len(binC[0]),len(binC[1])))
ground_truth_betaF_MAP[np.isnan(ground_truth_betaF_MAP)] = 0
betaF_MH_std = np.std(ground_truth_betaF_MH, axis=0)
ground_truth_betaF_MH = np.reshape(betaF_MH_std,(len(binC[0]),len(binC[1])))
ground_truth_betaF_MH[np.isnan(ground_truth_betaF_MH)] = 0

X,Y = np.meshgrid(binC[0],binC[1])
Z = np.reshape(betaF_MAP,(len(binC[0]),len(binC[1])))
Zmin = np.amin(Z[~np.isnan(Z)])
Zmax = np.amax(Z[~np.isnan(Z)])
Z[np.isnan(Z)] = 0
ax = axes_formal[1][0]
im = ax.contour(X, Y, Z.T, 40, cmap=cmap_setting, levels=np.arange(-15, 2).tolist())
colorbar_list[1][0] = fig_formal.colorbar(im, ax=ax)
ax = axes_formal[2][0]
im = ax.contour(X, Y, ground_truth_betaF_MAP.T, 40, cmap=cmap_setting, levels=np.arange(-15, 2).tolist())
colorbar_list[2][0] = fig_formal.colorbar(im, ax=ax)
ax = axes_formal[3][0]
im = ax.contour(X, Y, ((Z - np.mean(Z)) - (ground_truth_betaF_MAP - np.mean(ground_truth_betaF_MAP))).T, 40, 
    cmap=cmap_setting, levels=range(-10, 11))
colorbar_list[3][0] = fig_formal.colorbar(im, ax=ax)

X,Y = np.meshgrid(binC[0],binC[1])
betaF_MH_std = np.std(betaF_MH, axis=0)
Z = np.reshape(betaF_MH_std,(len(binC[0]),len(binC[1])))
Zmin = np.amin(Z[~np.isnan(Z)])
Zmax = np.amax(Z[~np.isnan(Z)])
Z[np.isnan(Z)] = 0
ax = axes_formal[1][1]
im = ax.contour(X, Y, Z.T, 40, cmap=cmap_setting)
colorbar_list[1][1] = fig_formal.colorbar(im, ax=ax)
ax = axes_formal[2][1]
im = ax.contour(X, Y, ground_truth_betaF_MH.T, 40, cmap=cmap_setting)
colorbar_list[2][1] = fig_formal.colorbar(im, ax=ax)
axes_formal[3][1].axis('off')

for item in range(2):
    ax = axes_formal[0][item]
    ax.set_xlabel("$\\xi_1$", fontsize = temp_font_size)
    ax.set_ylabel("$\\xi_2$", fontsize = temp_font_size)
for item in range(1, 4):
    for item_1 in range(2):
        ax = axes_formal[item][item_1]
        ax.set_aspect('equal')
        ax.set_xlabel(r"$\phi$ (rad)", fontsize = temp_font_size)
        ax.set_ylabel(r"$\psi$ (rad)", fontsize = temp_font_size)
temp_annotation = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)']
for item in range(7):
    ax = axes_formal[item / 2][item % 2]
    xlim = ax.get_xlim(); xlim_width = xlim[1] - xlim[0]
    ylim = ax.get_ylim(); ylim_width = ylim[1] - ylim[0]
    ax.text(xlim[0] - xlim_width * 0.15, ylim[1] - 0.05 * ylim_width, temp_annotation[item], fontsize=temp_font_size)
for item in range(3):
    colorbar_list[item][0].set_label('$\\beta F$', fontsize = temp_font_size)
    colorbar_list[item][1].set_label('$\\sigma_{\\beta F}$', fontsize = temp_font_size)

colorbar_list[3][0].set_label('$\\beta \\Delta F$', fontsize = temp_font_size)


fig_formal.set_size_inches(16, 28)
fig_formal.tight_layout()
fig_formal.savefig('betaF_contour.png')
