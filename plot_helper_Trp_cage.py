import matplotlib
matplotlib.use("agg")
import os, re, sys, time
import random, math
import numpy as np
import numpy.matlib
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt

target_folder = sys.argv[1]
cmap_setting = 'gist_rainbow'
temp_font_size = 25
colorbar_list = [[None,None] for item in range(4)]

f__hist_binCenters = target_folder + '/hist_binCenters.txt'
f__hist_binWidths = target_folder + '/hist_binWidths.txt'
f__pdf_MAP = target_folder + '/pdf_MAP.txt'
f__betaF_MAP = target_folder + '/betaF_MAP.txt'
f__f_MAP = target_folder + '/f_MAP.txt'
f__pdf_MH = target_folder + '/pdf_MH.txt'
f__betaF_MH = target_folder + '/betaF_MH.txt'
f__f_MH = target_folder + '/f_MH.txt'
f__logL_MH = target_folder + '/logL_MH.txt'
f__step_MH = target_folder + '/step_MH.txt'

binC = []
with open(f__hist_binCenters,'r') as fin:
    for line in fin:
        binC.append(line.strip().split())
binC = [[float(y) for y in x] for x in binC]
binC = np.array(binC)

binW = []
with open(f__hist_binWidths,'r') as fin:
    for line in fin:
        binW.append(line.strip().split())
binW = [[float(y) for y in x] for x in binW]
binW = np.array(binW)

fin = open(f__pdf_MAP,'r')
line = fin.readline()
pdf_MAP = line.strip().split()
pdf_MAP = [float(x) for x in pdf_MAP]
pdf_MAP = np.array(pdf_MAP)
fin.close()

fin = open(f__betaF_MAP,'r')
line = fin.readline()
betaF_MAP = line.strip().split()
betaF_MAP = [float(x) for x in betaF_MAP]
betaF_MAP = np.array(betaF_MAP)
fin.close()

fin = open(f__f_MAP,'r')
line = fin.readline()
f_MAP = line.strip().split()
f_MAP = [float(x) for x in f_MAP]
f_MAP = np.array(f_MAP)
fin.close()

pdf_MH = []
with open(f__pdf_MH,'r') as fin:
    for line in fin:
        pdf_MH.append(line.strip().split())
pdf_MH = [[float(y) for y in x] for x in pdf_MH]
pdf_MH = np.array(pdf_MH)

betaF_MH = []
with open(f__betaF_MH,'r') as fin:
    for line in fin:
        betaF_MH.append(line.strip().split())
betaF_MH = [[float(y) for y in x] for x in betaF_MH]
betaF_MH = np.array(betaF_MH)

f_MH = []
with open(f__f_MH,'r') as fin:
    for line in fin:
        f_MH.append(line.strip().split())
f_MH = [[float(y) for y in x] for x in f_MH]
f_MH = np.array(f_MH)

logL_MH = []
with open(f__logL_MH,'r') as fin:
    for line in fin:
        logL_MH.append(line.strip())
logL_MH = [float(x) for x in logL_MH]
logL_MH = np.array(logL_MH)

step_MH = []
with open(f__step_MH,'r') as fin:
    for line in fin:
        step_MH.append(line.strip())
step_MH = [float(x) for x in step_MH]
step_MH = np.array(step_MH)

fig_formal, axes_formal = plt.subplots(2, 2)

X,Y = np.meshgrid(binC[0],binC[1])
Z = np.reshape(betaF_MAP,(len(binC[0]),len(binC[1])))
np.save('betaF_MAP', Z)
Zmin = np.amin(Z[~np.isnan(Z)])
Zmax = np.amax(Z[~np.isnan(Z)])
Z[np.isnan(Z)] = 0
ax = axes_formal[0][0]
im = ax.contour(X, Y, Z.T, 40, cmap=cmap_setting)
colorbar_list[0][0] = fig_formal.colorbar(im, ax=ax)

betaF_MH_std = np.std(betaF_MH, axis=0)

X,Y = np.meshgrid(binC[0],binC[1])
Z = np.reshape(betaF_MH_std,(len(binC[0]),len(binC[1])))
Zmin = np.amin(Z[~np.isnan(Z)])
Zmax = np.amax(Z[~np.isnan(Z)])
Z[np.isnan(Z)] = 0
ax = axes_formal[0][1]
im = ax.contour(X, Y, Z.T, 40, cmap=cmap_setting)
colorbar_list[0][1] = fig_formal.colorbar(im, ax=ax)

#########################################

f__hist_binCenters = target_folder + '/hist_binCenters_PROJ.txt'
f__hist_binWidths = target_folder + '/hist_binWidths_PROJ.txt'
f__pdf_MAP = target_folder + '/pdf_PROJ_MAP.txt'
f__betaF_MAP = target_folder + '/betaF_PROJ_MAP.txt'
f__pdf_MH = target_folder + '/pdf_PROJ_MH.txt'
f__betaF_MH = target_folder + '/betaF_PROJ_MH.txt'

binC = []
with open(f__hist_binCenters,'r') as fin:
    for line in fin:
        binC.append(line.strip().split())
binC = [[float(y) for y in x] for x in binC]
binC = np.array(binC)

binW = []
with open(f__hist_binWidths,'r') as fin:
    for line in fin:
        binW.append(line.strip().split())
binW = [[float(y) for y in x] for x in binW]
binW = np.array(binW)

fin = open(f__pdf_MAP,'r')
line = fin.readline()
pdf_MAP = line.strip().split()
pdf_MAP = [float(x) for x in pdf_MAP]
pdf_MAP = np.array(pdf_MAP)
fin.close()

fin = open(f__betaF_MAP,'r')
line = fin.readline()
betaF_MAP = line.strip().split()
betaF_MAP = [float(x) for x in betaF_MAP]
betaF_MAP = np.array(betaF_MAP)
fin.close()

pdf_MH = []
with open(f__pdf_MH,'r') as fin:
    for line in fin:
        pdf_MH.append(line.strip().split())
pdf_MH = [[float(y) for y in x] for x in pdf_MH]
pdf_MH = np.array(pdf_MH)

betaF_MH = []
with open(f__betaF_MH,'r') as fin:
    for line in fin:
        betaF_MH.append(line.strip().split())
betaF_MH = [[float(y) for y in x] for x in betaF_MH]
betaF_MH = np.array(betaF_MH)

X,Y = np.meshgrid(binC[0],binC[1])
Z = np.reshape(betaF_MAP,(len(binC[0]),len(binC[1])))
np.save('X', X)
np.save('Y', Y)
np.save('betaF_MAP_proj', Z)
Zmin = np.amin(Z[~np.isnan(Z)])
Zmax = np.amax(Z[~np.isnan(Z)])
Z[np.isnan(Z)] = 0
ax = axes_formal[1][0]
im = ax.contour(X, Y, Z.T, 40, cmap=cmap_setting)
colorbar_list[1][0] = fig_formal.colorbar(im, ax=ax)

X,Y = np.meshgrid(binC[0],binC[1])
betaF_MH_std = np.std(betaF_MH, axis=0)
Z = np.reshape(betaF_MH_std,(len(binC[0]),len(binC[1])))
Zmin = np.amin(Z[~np.isnan(Z)])
Zmax = np.amax(Z[~np.isnan(Z)])
Z[np.isnan(Z)] = 0
ax = axes_formal[1][1]
im = ax.contour(X, Y, Z.T, 40, cmap=cmap_setting)
colorbar_list[1][1] = fig_formal.colorbar(im, ax=ax)

for item in range(2):
    ax = axes_formal[0][item]
    ax.set_xlabel("$\\xi_1$", fontsize = temp_font_size)
    ax.set_ylabel("$\\xi_2$", fontsize = temp_font_size)

temp_annotation = ['(a)', '(b)', '(c)', '(d)']

for item in range(1, 2):
    for item_1 in range(2):
        ax = axes_formal[item][item_1]
        ax.set_xlabel("$C_\\alpha$ RMSD / $\AA$", fontsize = temp_font_size)
        ax.set_ylabel("helix RMSD / $\AA$", fontsize = temp_font_size)
for item in range(4):
    ax = axes_formal[item / 2][item % 2]
    xlim = ax.get_xlim(); xlim_width = xlim[1] - xlim[0]
    ylim = ax.get_ylim(); ylim_width = ylim[1] - ylim[0]
    ax.text(xlim[0] - xlim_width * 0.15, ylim[1] - 0.05 * ylim_width, temp_annotation[item], fontsize=temp_font_size)
for item in range(2):
    colorbar_list[item][0].set_label('$\\beta F$', fontsize = temp_font_size)
    colorbar_list[item][1].set_label('$\\sigma_{\\beta F}$', fontsize = temp_font_size)

fig_formal.set_size_inches(16, 14)

fig_formal.tight_layout()
fig_formal.savefig('betaF_contour.png')
